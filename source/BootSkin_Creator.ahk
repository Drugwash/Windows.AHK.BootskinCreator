; © Drugwash, 2014-2023
;==========================================================
;	DIRECTIVES
;==========================================================
#NoEnv
#SingleInstance Force
ListLines, Off
SetBatchLines -1
SetControlDelay, -1
SetWinDelay, -1
DetectHiddenWindows, On
;==========================================================
;	IDENTIFICATION
;==========================================================
author=Drugwash
appname=BootSkin Creator
version=1.0.0.8
releaseD=August 17, 2023
releaseT=Alpha
debug=0
;==========================================================
;	VARIABLES
;==========================================================
AStr := A_IsUnicode ? AStr : "Str"	; until we switch to updates.ahk for AHK_L compatibility
iconlocal=%A_ScriptDir%\res\%appname%.ico
wd := A_Temp "\" appname
src := wd "\src"
FileRemoveDir, %src%, 1
FileCreateDir, %wd%
FileCreateDir, %src%
SetWorkingDir, %wd%
FileInstall, res\7z.exe, %wd%\7z.exe
inifile := A_ScriptDir "\" appname " preferences.ini"
inikeys := "Type,Screen,ProgressBar,ProgressBarX,ProgressBarY
,ProgressBarWidth,ProgressBarHeight,Name,Author,Description"	; skin keys in ini file
editkeys := "Name,Author,URL,Date,Modder,ModTool
	,SkinCode,ModCode,Version,BuildType"						; editor keys in ini file
comp := "none|RLE 8|RLE 4|Bit field|JPEG|PNG"					; bitmap compression types
labels1 := "Name,Author,URL,Date,Modded by,Mod tool,Valid,"		; mini-preview (ini)
labels2 := "Type,Size,Colors,Compression,Resolution,File size,Valid,Palette" ; mini-preview (bmp)
labels3 := "Type,Size,Colors,Format,Bytes/color,File size,,Palette"	; mini-preview (pal)
labels4 := "Type,Format,Encoding,Date,Attributes,File size,,Open"	; mini-preview (other)
fscr=0			; fullscreen switch
stop=1			; preview animation switch
bmH := 130		; mini-preview bitmap height
bmW := bmH*4/3	; mini-preview bitmap width
;times=0		; temporary var to catch cursor hide bug
RegRead, bsdir, HKLM, Software\Stardock\WinCustomize\BootSkin, Path	; BootSkin install dir
RegRead, bsver, HKLM, Software\Stardock\WinCustomize\BootSkin, Version	; Revision, EXE
;==========================================================
;	PREFERENCES
;==========================================================
IniRead, aot, %inifile%, Preferences, AlwaysOnTop, 0
IniRead, autolast, %inifile%, Preferences, AutoLoadLast, 0
IniRead, resMgn, %inifile%, Preferences, ResizeMargin, 3
IniRead, hcTime, %inifile%, Preferences, CursorTimeout, 3500
IniRead, delay, %inifile%, Preferences, AnimationDelay, 120
IniRead, lastfolder, %inifile%, User, LastFolder, %A_ScriptDir%
;==========================================================
;	TRAY  MENU
;==========================================================
Menu, Tray, UseErrorLevel
Menu, Tray, Icon, % A_IsCompiled ? "" : iconlocal
Menu, Tray, Tip, %appname% %version% (%releaseT%)
Menu, Tray, NoStandard
Menu, Tray, Add, Settings, options
Menu, Tray, Add, AlwaysOnTop, aot
Menu, Tray, Default, AlwaysOnTop
Menu, Tray, % (aot ? "Check" : "Uncheck"), AlwaysOnTop
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, GuiClose
;==========================================================
;	MAIN  MENU
;==========================================================
Menu, MenuLevel4, Add, How &to..., testlabel
Menu, MenuLevel4, Add, &About, testlabel
Menu, MenuLevel3, Add, Always on &top, aot
Menu, MenuLevel3, % (aot ? "Check" : "Uncheck"), Always on &top
Menu, MenuLevel3, Add, Keep &backups, testlabel
Menu, MenuLevel3, Add, Auto-&Clear list, testlabel
Menu, MenuLevel3, Add, Auto-&Load last, autolast
Menu, MenuLevel3, % (autolast ? "Check" : "Uncheck"), Auto-&Load last
Menu, MenuLevel3, Add, Auto-&Sort skins, testlabel
Menu, MenuLevel3, Add
Menu, MenuLevel3, Add, Image &editor, testlabel
Menu, MenuLevel3, Add, &Default folder, testlabel
Menu, MenuLevel2, Add, &New skin, testlabel
Menu, MenuLevel2, Add
Menu, MenuLevel2, Add, &Open skin(s), selSkins
Menu, MenuLevel2, Add, Open &folder, selFolder
Menu, MenuLevel2, Add
Menu, MenuLevel2, Add, A&dd to current, selFiles
Menu, MenuLevel2, Add, &Remove file(s), testlabel
Menu, MenuLevel2, Add
Menu, MenuLevel2, Add, &Save skin, testlabel
Menu, MenuLevel2, Add, Save skin &as, testlabel
Menu, MenuLevel2, Add
Menu, MenuLevel2, Add, E&xit, testlabel
Menu, MyMenu, Add, &File, :MenuLevel2
Menu, MyMenu, Add, &Preferences, :MenuLevel3
Menu, MyMenu, Add, &Help, :MenuLevel4
;==========================================================
;	MAIN  GUI
;==========================================================
Gui, 1:Menu, MyMenu
Gui, 1:+OwnDialogs
Gui, 1:Font, s8, Tahoma
Gui, 1:Add, GroupBox, x4 y2 w260 h250, Skin list:
Gui, 1:Add, ListView, x8 y17 w252 h231 -LV0x10 +LV0x4400 +0x8 -Multi Sort AltSubmit vLV1 gLV1action
	, Skin name|Ext|Full path|dummy
Gui, 1:Add, GroupBox, x4 y256 w260 h139, Skin contents:
Gui, 1:Add, ListView, x8 y270 w252 h122 -LV0x10 +LV0x4400 +0x8 AltSubmit vLV2 gLV2action
	, File name|Ext|Full path|dummy
Gui, 1:Add, Button, x4 y398 w62 h24 vtev gtoggleMode, Edit
Gui, 1:Add, Button, x70 y398 w62 h24 +Disabled, Save
Gui, 1:Add, Button, x136 y398 w62 h24 Disabled vsaveas, Save as
Gui, 1:Add, Button, x202 y398 w62 h24 Default Disabled vpvw gpreview, Preview
Gui, 1:Add, GroupBox, x269 y3 w368 h37, Package name:
Gui, 1:Add, Checkbox, x273 y18 w94 h18 Disabled vtini0 gtini0, Same as skin
Gui, 1:Add, Edit, x368 y18 w265 h18 -Multi +ReadOnly vini0,
Gui, 1:Add, GroupBox, x269 y44 w368 h96, Technical data:
Gui, 1:Add, Text, x273 y58 w94 h18 +Right +0x200, Type :
Gui, 1:Add, Edit, x368 y58 w30 h18 -Multi +Limit3 +ReadOnly vini1,
Gui, 1:Add, Text, x401 y58 w140 h18 Disabled +0x200, (no known override values)
Gui, 1:Add, Text, x273 y78 w94 h18 +Right +0x200, Background img. :
Gui, 1:Add, Edit, x368 y78 w244 h18 -Multi +ReadOnly vini2,
Gui, 1:Add, Button, x612 y78 w21 h18 Disabled vbrbkg, 
Gui, 1:Add, Checkbox, x273 y98 w94 h18 Disabled vtini3 gtini3, %A_Space%Progressbar* :
Gui, 1:Add, Edit, x368 y98 w244 h18 -Multi +ReadOnly vini3,
Gui, 1:Add, Button, x612 y98 w21 h18 Disabled vbrprg, 
Gui, 1:Add, Text, x273 y118 w94 h18 +Right +0x200, Moving range  X:
Gui, 1:Add, Edit, x368 y118 w30 h18 -Multi +Limit3 +ReadOnly vini4,
Gui, 1:Add, Text, x400 y118 w16 h18 +Right +0x200, Y:
Gui, 1:Add, Edit, x416 y118 w30 h18 -Multi +Limit3 +ReadOnly vini5,
Gui, 1:Add, Text, x448 y118 w16 h18 +Right +0x200, W:
Gui, 1:Add, Edit, x464 y118 w30 h18 -Multi +Limit3 +ReadOnly vini6,
Gui, 1:Add, Text, x496 y118 w16 h18 +Right +0x200, H:
Gui, 1:Add, Edit, x512 y118 w30 h18 -Multi +Limit3 +ReadOnly Disabled vini7,
Gui, 1:Add, Checkbox, x543 y118 w90 h18 Disabled vtini7 +Right gtini7, Custom height
Gui, 1:Add, GroupBox, x269 y144 w368 h108, Friendly info:
Gui, 1:Add, Text, x273 y158 w94 h18 +Right +0x200, Skin name :
Gui, 1:Add, Edit, x368 y158 w265 h18 -Multi +ReadOnly vini8,
Gui, 1:Add, Checkbox, x273 y178 w94 h18 Disabled vtini9 gtini9, %A_Space%%A_Space%Skin author* :
Gui, 1:Add, Edit, x368 y178 w265 h18 -Multi +ReadOnly vini9,
Gui, 1:Add, Checkbox, x273 y198 w94 h18 Disabled vtini10 gtini10, %A_Space%%A_Space%Description* :
Gui, 1:Add, Edit, x368 y198 w265 h50 +Multi +ReadOnly vini10,
Gui, 1:Font, s7
Gui, 1:Add, Text, x294 y224 w65 h24 -Right +Disabled, * optional element/field
Gui, 1:Font
Gui, 1:Add, GroupBox, x269 y256 w368 h139 vgbimg, Editor info:
Gui, 1:Add, Text, x273 y270 w94 h14 +0x200 +Right vlbl1, Name :
Gui, 1:Add, Text, xp y285 wp hp +0x200 +Right vlbl2, Author :
Gui, 1:Add, Text, xp y300 wp hp +0x200 +Right vlbl3, URL :
Gui, 1:Add, Text, xp y315 wp hp +0x200 +Right vlbl4, Date :
Gui, 1:Add, Text, xp y330 wp hp +0x200 +Right vlbl5, Modded by :
Gui, 1:Add, Text, xp y345 wp hp +0x200 +Right vlbl6, Mod tool :
Gui, 1:Add, Text, xp y360 wp hp +0x200 cBlue +Right vlbl7, Valid :
Gui, 1:Add, Text, xp y375 wp hp +0x200 +Right Hidden vbml1, Palette :
Gui, 1:Add, Text, x+1 y270 w265 h14 +0x200 Section ved1,
Gui, 1:Add, Text, xp y285 wp h14 +0x200 ved2,
Gui, 1:Font, s7 CBlue Underline
Gui, 1:Add, Text, xp y300 wp h14 +0x200 ved3 gedlink,
Gui, 1:Font
Gui, 1:Add, Text, xp y315 wp h14 +0x200 ved4,
Gui, 1:Add, Text, xp y330 wp h14 +0x200 ved5,
Gui, 1:Add, Text, xp y345 wp h14 +0x200 ved6,
Gui, 1:Add, Text, xp y360 w30 h14 +0x200 ved7,
Gui, 1:Add, Text, x+5 yp w30 h14 +0x200 ved8,
Gui, 1:Add, Picture, xs y+1 w13 h13 AltSubmit Border Hidden vbml2 gshowpal
	, %A_ScriptDir%\res\pal16.bmp
Gui, 1:Add, Picture, x460 ys-6  w%bmW% h%bmH% AltSubmit Hidden vpic,
Gui, 1:Add, Checkbox, x273 y375 w160 h18 Disabled vtedit0, Save editor info to .ini file
Gui, 1:Add, Button, x269 y398 w65 h24 +Disabled, Fix image
Gui, 1:Add, Button, x+5 y398 w55 h24 +Disabled, Fix .ini
Gui, 1:Add, Button, x460 y398 w55 h24 +Disabled ginstall, Install	; copy to BootSkin skins folder
Gui, 1:Add, Button, x+4 y398 w55 h24, Apply			; create and install driver & registry
Gui, 1:Add, Button, x+4 y398 w55 h24, Exit
Gui, 1:Add, Progress, x70 y3 w129 h23 cRed Hidden hwndhPrg vldprg, 0
Gui, 1:Add, Slider, x203 y3 w62 h23 AltSubmit Thick18 Center NoTicks Line1 Page10 Range20-1000 ToolTip hwndhDly vdelay gdelay, %delay%
Gui, 1:Add, StatusBar,,
Gui, 1:Default
SB_SetParts(67, 133, 66)
SB_SetText("`tView mode", 1)
SB_SetText("`tReady", 2)
SB_SetText("`tNo error", 4)
GuiControlGet, hSB, 1:Hwnd, msctls_statusbar321
SendMessage, 0x408, 25, 0,, ahk_id %hSB%			; SB_SETMINHEIGHT
DllCall("SetParent", "UInt", hPrg, "UInt", hSB)
DllCall("SetParent", "UInt", hDly, "UInt", hSB)
LV_ModifyCol(4, 0)
Gui, ListView, LV1
LV_ModifyCol(4, 0)
LV_ModifyCol(2, 30)
; Generated using SmartGuiXP Creator mod 4.3.29.3
Gui, 1:Show, Center w640 h450, %appname%
WinGet, hMain, ID, %appname%
;==========================================================
;	PREVIEW  GUI
;==========================================================
Gui, 2:Color, Black, Black
Gui, 2:Margin, 0, 0
Gui, 2:-Caption +ToolWindow +Border +OwnDialogs +AlwaysOnTop
Gui, 2:Add, Picture, x0 y0 w2 h2 BackgroundTrans AltSubmit vpvw1 gmoveit,
Gui, 2:Add, Picture, x0 y0 w2 h2 BackgroundTrans AltSubmit vpvw2,
Gui, 2:Add, Picture, x0 y0 w20 h20 +0x8 BackgroundTrans AltSubmit Hidden hwndhpvw3 vpvw3 gmoveit,
; Generated using SmartGuiXP Creator mod 4.3.29.3
Gui, 2:Show, Hide Center w640 h480, Preview
WinGet, hPvw, ID, Preview
;==========================================================
;	TEXT  GUI
;==========================================================
Gui, 3:Font, s8, Tahoma
Gui, 3:Margin, 2, 2
Gui, 3:+Resize +MinSize256x256
Gui, 3:Add, Edit, w252 h252 ReadOnly hwndhTxt vtxt,
Gui, 3:Show, Hide AutoSize, Text
WinGet, hTxtWnd, ID, Text
;==========================================================
;	PALETTE GUI
;==========================================================
Gui, 4:Color, Black, Black
Gui, 4:Font, cWhite
Gui, 4:Margin, 2, 2
Gui, 4:Add, Text, w150 h30 +0x31100 +E0x4 hwndhPal gSelect,
Gui, 4:Add, Text, xp y+2 wp h16 +0x1100 +E0x4,
Gui, 4:Add, Edit, y+2 wp h20 ReadOnly,
Gui, 4:Show, Hide AutoSize, Palette
WinGet, hPalWnd, ID, Palette
;==========================================================
Gui, 5:Add, Text, w300 h16,
Gui, 5:Add, Text, w300 h16,
Gui, 5:Add, Text, w300 h16,
Gui, 5:Add, Text, w300 h16,
Gui, 5:Show, % (debug ? "" : "Hide"), Debug
;==========================================================
GuiControlGet, ppos, 1:Pos, pic
hCursH := DllCall("LoadCursor", "UInt", NULL, "Int", 32649, "UInt")	; IDC_HAND
hCursA := DllCall("LoadCursor", "UInt", NULL, "Int", 32646, "UInt")	; IDC_SIZEALL
hCursN := DllCall("LoadCursor", "UInt", NULL, "Int", 32648, "UInt")	; IDC_NO
hCursZ := DllCall("LoadCursor", "UInt", NULL, "Int", 32644, "UInt")	; IDC_SIZEWE
hCursV := DllCall("LoadCursor", "UInt", NULL, "Int", 32645, "UInt")	; IDC_SIZENS
hCursTL := DllCall("LoadCursor", "UInt", NULL, "Int", 32642, "UInt")	; IDC_SIZENWSE
hCursTR := DllCall("LoadCursor", "UInt", NULL, "Int", 32643, "UInt")	; IDC_SIZENESW
OnMessage(0x200, "WM_MOUSEMOVE")	; WM_MOUSEMOVE
OnMessage(0x201, "WM_LBUTTONDOWN")	; WM_LBUTTONDOWN
OnMessage(0x202, "WM_LBUTTONUP")		; WM_LBUTTONUP
OnMessage(0x203, "WM_LBUTTONDBLCLK")	; WM_LBUTTONDBLCLK
OnMessage(0x207, "WM_LBUTTONDBLCLK")	; WM_MBUTTONDOWN
WPNew := RegisterCallback("WindowProc", "F", 4, "")
WPOld := DllCall("SetWindowLong", "UInt", hPal, "Int", -4, "Int", WPNew, "UInt")
OnExit, exit
if autolast
	gosub loadFolder
testlabel:
; Automatic label for Menu commands
Return
;==========================================================
;	EXIT
;==========================================================
GuiClose:
ButtonExit:
; add check for unsaved work!!!
ExitApp

reload:
; add check for unsaved work!!!
Reload
Sleep, 2000
ExitApp

exit:
IniWrite, %aot%, %inifile%, Preferences, AlwaysOnTop
IniWrite, %autolast%, %inifile%, Preferences, AutoLoadLast
IniWrite, %hcTime%, %inifile%, Preferences, CursorTimeout
IniWrite, %delay%, %inifile%, Preferences, AnimationDelay
IniWrite, %lastfolder%, %inifile%, User, LastFolder
OnMessage(0x200,"")
OnMessage(0x201,"")
OnMessage(0x202,"")
OnMessage(0x203,"")
OnMessage(0x207,"")
if fscr
	gosub restoreFS
SetWorkingDir, %A_ScriptDir%
IfExist, %wd%
	FileRemoveDir, %wd%, 1
DllCall("DestroyCursor", "UInt", hCursH)
DllCall("DestroyCursor", "UInt", hCursA)
DllCall("DestroyCursor", "UInt", hCursN)
DllCall("DestroyCursor", "UInt", hCursZ)
DllCall("DestroyCursor", "UInt", hCursV)
DllCall("DestroyCursor", "UInt", hCursTL)
DllCall("DestroyCursor", "UInt", hCursTR)
ExitApp
;==========================================================
;	DROP FILES
;==========================================================
GuiDropFiles:
if A_GuiControl=LV1
	goto dropSkins
else if A_GuiControl=LV2
	goto dropFiles
return
;==========================================================
;	RESIZE TEXT WINDOW
;==========================================================
3GuiSize:
GuiControl, 3:Move, txt, % "w" A_GuiWidth-4 " h" A_GuiHeight-4
return
;==========================================================
;	DRAG-TO-MOVE PREVIEW WINDOW
;==========================================================
moveit:
MouseGetPos, mx, my, wind, ctrl, 0	; use 2 to get hwnd
if wind not in %hPvw%
	return
if ctrl in Static1
	{
	DllCall("SetCursor", "UInt", hCursA)
	PostMessage, 0xA1, 2,,, Preview
	}
else if ctrl in Static2
	DllCall("SetCursor", "UInt", mode ? hCursH : hCursN)
return
;==========================================================
;	TOGGLE PREVIEW FULLSCREEN
;==========================================================
switchFS:
;if !disp := FullScreen_Set(hPvw, "640x480x4", 1)
if !disp := FullScreen_Set(hPvw, "640x480x32x85", 1)	; last param is window border
	return
fscr=1
GuiControl, 2:-g, pvw1
return

restoreFS:
if fscr
	{
	FullScreen_Reset(hPvw, disp)
	GuiControl, 2:+gmoveit, pvw1
	}
fscr=0
return

2GuiContextMenu:
2GuiEscape:
gosub restoreFS
if !stop
	gosub preview
Gui, 2:Hide
return
;==========================================================
;	TOGGLE ALWAYS-ON-TOP
;==========================================================
aot:
aot := !aot
Gui, % "1:" (aot ? "+" : "-") "AlwaysOnTop"
Menu, Tray, ToggleCheck, AlwaysOnTop
Menu, MenuLevel3, ToggleCheck, Always on &top
return
;==========================================================
;	TOGGLE AUTO-LOAD LAST FOLDER
;==========================================================
autolast:
autolast := !autolast
Menu, MenuLevel3, ToggleCheck, Auto-&Load last
return
;==========================================================
;	SKIN LIST ACTION
;==========================================================
LV1action:
;Critical
if (!(A_GuiEvent="I" && InStr(ErrorLevel, "S", TRUE)) OR A_EventInfo=r1)
	return
r1 := A_EventInfo				; focused row
;stop=1						; reset Preview state
;SetTimer, endless, Off			; break preview loop
if mode
	gosub toggleMode	; better check for changes and prompt to save first !!!
GuiControl, 1:Enable, pvw
GuiControl, 1:, pvw, Preview
;Gui, 2:Hide
Gui, 3:Hide
Gui, 4:Hide
Gui, ListView, LV1
LV_GetText(file, r1, 3)
LV_GetText(ex, r1, 2)
LV_GetText(pkg, r1, 1)
file .= "\" pkg "." ex
GuiControl, 1:, gbimg, Editor info:
Loop, Parse, labels1, CSV
	{
	GuiControl, 1:, lbl%A_Index%, % A_LoopField ? A_LoopField " :" : ""
	GuiControl, 1:Move, ed%A_Index%, w265
	GuiControl, 1:, ed%A_Index%,
	}
Gui, 1:Font, s7 cBlue Underline
GuiControl, 1:Font, ed3
Gui, 1:Font
GuiControl, 1:+g, ed3
GuiControl, 1:Hide, pic
GuiControl, 1:Hide, bml1
GuiControl, 1:Hide, bml2
GuiControl, 1:Show, ed8
GuiControl, 1:Show, tedit0
SetTimer, unfold, -1
return

unfold:
gosub listFiles
if !stop OR stop=2
	{
	stop=1
	gosub preview
	}
return
;==========================================================
;	SKIN CONTENTS ACTION
;==========================================================
LV2action:
;Critical
if (!(A_GuiEvent="I" && InStr(ErrorLevel, "S", TRUE)) OR A_EventInfo=r2)
	return
r2 := A_EventInfo		; focused row
Gui, ListView, LV2
LV_GetText(i, r2, 1)
LV_GetText(e, r2, 2)
LV_GetText(file, r2, 3)
file .= "\" i "." e
GuiControl, 1:Enable, saveas
if e in bmp,jpg,jpeg,png,gif,tga		; picture selected
	{
	SetTimer, loadBmp, -1			; show bitmap(s) in minipreview window
	return
	}
if (i="bootskin" && e="ini")			; selected bootskin.ini
	{
	j := i "." e						; required by loadIni
	SetTimer, showIni, -1
	return
	}
if e in act,pal						; selected Adobe Color Table
	{
	SetTimer, showPalette, -1
	return
	}
; fallback for any other kind of file
; we should check MIME types and return info with option to open in default app.
GuiControl, 1:, gbimg, File info:		; might change to 'File info' at some point
Loop, Parse, labels4, CSV
	{
	GuiControl, 1:, lbl%A_Index%, % A_LoopField ? A_LoopField " :" : ""
	GuiControl, 1:Move, ed%A_Index%, w90
	}
FileRead, txt, %file%
Gui, 3:Show
GuiControl, 3:, txt, %txt%
;SendMessage, 0xB1, -1, 0,, ahk_id %hTxt%	; EM_SETSEL (unselect all)
return

showPalette:
gosub buildACT
goto showpal
return

showIni:
GuiControl, 1:, gbimg, Editor info:
Loop, Parse, labels1, CSV
	{
	GuiControl, 1:, lbl%A_Index%, % A_LoopField ? A_LoopField " :" : ""
	GuiControl, 1:Move, ed%A_Index%, w265
	if A_Index between 2 and 6
		GuiControl, 1:, ed%A_Index%, % cini%A_Index%
	}
GuiControl, 1:, ed1, %cini1% %cini9% %cini10%
GuiControl, 1:, ed7, % (cini7 <> test1 ? "NO" : "YES")
GuiControl, 1:, ed8, % (cini8 <> test2 ? "NO" : "YES")
Gui, 1:Font, s7 cBlue Underline
GuiControl, 1:Font, ed3
Gui, 1:Font
GuiControl, 1:+g, ed3
GuiControl, 1:Hide, pic
GuiControl, 1:Hide, bml1
GuiControl, 1:Hide, bml2
GuiControl, 1:Show, ed8
GuiControl, 1:Show, tedit0
gosub loadIni
Gui, 1:Submit, NoHide
return
;==========================================================
;	ADD FILES TO CURRENT SKIN
;==========================================================
selFiles:
Gui, 1:+OwnDialogs
FileSelectFile, i, M3, %lastfile%, Select file(s):, Bitmap files (*.bmp)
if (!i OR ErrorLevel)
	return
SB_SetText("`tAdding file(s)...", 2)
Gui, ListView, LV2
gosub addToList
LV_ModifyCol(1, "AutoHdr")
LV_ModifyCol(2, "AutoHdr Logical Sort")
;Gui, ListView, LV1
SB_SetText("`tReady", 2)
GuiControl, 1:Hide, ldprg
return

dropFiles:
i := A_GuiEvent
SB_SetText("`tAdding file(s)...", 2)
Gui, ListView, LV2
Loop, Parse, i, `n
	{
	SplitPath, A_LoopField, j, root, e, k
	LV_Add("", k, e, root)
	}
LV_ModifyCol(1, "AutoHdr")
LV_ModifyCol(2, "AutoHdr Logical Sort")
;Gui, ListView, LV1
SB_SetText("`tReady", 2)
GuiControl, 1:Hide, ldprg
return
;==========================================================
;	ADD SKIN(S) TO LIST
;==========================================================
selSkins:
Gui, 1:+OwnDialogs
FileSelectFile, i, M3, %lastfile%, Select file(s):, BootSkin files (*.bootskin)|Zip files (*.zip)
if (!i OR ErrorLevel)
	return
Gui, ListView, LV1
if acl					; if Auto-Clear list is enabled, clear list before loading items
	LV_Delete()

addToList:
if !InStr(i, "`n")		; single item selected
	{
	SplitPath, i, j, root, e, k
	LV_Add("", k, e, root)
	}
else
	{
	StringReplace, i, i, `n, `n, UseErrorLevel
	cnt := ErrorLevel
	GuiControl, 1:Range0-%cnt%, ldprg
	GuiControl, 1:, ldprg, 0
	GuiControl, 1:Show, ldprg
	 Loop, Parse, i, `n	; multiple items selected
		{
		if A_Index=1
			root := A_LoopField (StrLen(A_LoopField) > 3 ? "\" : "")
		else
			{
			SplitPath, A_LoopField, j, root, e, k
			LV_Add("", k, e, root)
			GuiControl, 1:, ldprg, +1
			}
		}
	}
LV_ModifyCol(1, "AutoHdr")
LV_ModifyCol(2, "AutoHdr")
i := LV_GetCount()
GuiControl, 1:, Button1, Skin list (%i%):
return

selFolder:
Gui, 1:+OwnDialogs
FileSelectFolder, i, *%lastfolder%, 3, Select skins folder:
if (!i OR ErrorLevel)
	return
lastfolder := i

loadFolder:
SB_SetText("`tAdding skins...", 2)
Gui, ListView, LV1
if acl					; if Auto-Clear list is enabled, clear list before loading items
	LV_Delete()
cnt=0
Loop, %lastfolder%\*.*, 0, 0
	{
	SplitPath, A_LoopFileLongPath,,, e,
	if e not in zip,bootskin
		continue
	cnt++
	}
GuiControl, 1:Range0-%cnt%, ldprg
GuiControl, 1:, ldprg, 0
GuiControl, 1:Show, ldprg
Loop, %lastfolder%\*.*, 0, 0
	{
	SplitPath, A_LoopFileLongPath, j, root, e, k
	if e not in zip,bootskin
		continue
	LV_Add("", k, e, root)
	GuiControl, 1:, ldprg, +1
	}
LV_ModifyCol(1, "AutoHdr")
LV_ModifyCol(2, "AutoHdr")
SB_SetText("`tReady", 2)
GuiControl, 1:Hide, ldprg
i := LV_GetCount()
GuiControl, 1:, Button1, Skin list (%i%):
return

dropSkins:
i := A_GuiEvent
SB_SetText("`tAdding skins...", 2)
Gui, ListView, LV1
if acl					; if Auto-Clear list is enabled, clear list before loading items
	LV_Delete()
StringReplace, i, i, `n, `n, UseErrorLevel
cnt := ErrorLevel+1
GuiControl, 1:Range0-%cnt%, ldprg
GuiControl, 1:, ldprg, 0
GuiControl, 1:Show, ldprg
Loop, Parse, i, `n
	{
	SplitPath, A_LoopField, j, root, e, k
	LV_Add("", k, e, root)
	GuiControl, 1:, ldprg, +1
	}
LV_ModifyCol(1, "AutoHdr")
LV_ModifyCol(2, "AutoHdr")
SB_SetText("`tReady", 2)
GuiControl, 1:Hide, ldprg
i := LV_GetCount()
GuiControl, 1:, Button1, Skin list (%i%):
return
;==========================================================
;	LIST SKIN CONTENTS
;==========================================================
listFiles:
SB_SetText("`tUnpacking...", 2)
gosub unpack
SB_SetText("`tCleanup...", 2)
gosub clean
SB_SetText("`tListing files...", 2)
Gui, ListView, LV2
LV_Delete()
Loop, %src%\*.*, 0, 0
	{
	if !A_LoopFileName
		break
	SplitPath, A_LoopFileLongPath, j, root, e, k
	if (j="bootskin.ini")
		{
		gosub loadIni
;		gosub codeValidation
;		gosub iniValidation
;		gosub bmpValidation
;		gosub checkmarks
		if mode		; only allow enabling when in Edit mode
			{
			GuiControl, % "1:" (pkg=ini8 ? "Disable" : "Enable"), ini0
			GuiControl, % "1:" (ini3="" ? "Disable" : "Enable"), tini7
			GuiControl, % "1:" (ini3="" ? "Disable" : "Enable"), brprg
			GuiControl, % "1:" (ini3="" ? "Disable" : "Enable"), ini3
			GuiControl, % "1:" (ini3="" ? "Disable" : "Enable"), ini4
			GuiControl, % "1:" (ini3="" ? "Disable" : "Enable"), ini5
			GuiControl, % "1:" (ini3="" ? "Disable" : "Enable"), ini6
			GuiControl, % "1:" (ini7="" ? "Disable" : "Enable"), ini7
			GuiControl, % "1:" (ini9="" ? "Disable" : "Enable"), ini9
			GuiControl, % "1:" (ini10="" ? "Disable" : "Enable"), ini10
			}
		Gui, 1:Submit, NoHide
		}
	LV_Add("", k, e, root)
	}
LV_ModifyCol(1, "AutoHdr")
LV_ModifyCol(2, "AutoHdr Logical Sort")
Loop, % LV_GetCount()
	{
	LV_GetText(i, A_Index, 1)
	LV_GetText(e, A_Index, 2)
	file := i "." e
	if (file=ini2)
		{
		LV_Modify(A_Index, "Vis Select")
		r2 := A_Index
		file := src "\" file
		gosub loadBmp
		GuiControl, 1:Enable, saveas
		break	; use this to keep e='ini' for hand cursor in URL (WM_MOUSEMOVE)
		}
	}
SB_SetText("`tReady", 2)
SB_SetText("`tView mode", 1)
return

loadIni:
Loop, Parse, inikeys, CSV
	{
	IniRead, ini%A_Index%, %root%\%j%, BootSkin, %A_LoopField%, %A_Space%
	GuiControl, 1:, ini%A_Index%, % ini%A_Index%
	}
Loop, Parse, editkeys, CSV
	{
	IniRead, cini%A_Index%, %root%\%j%, Editor, %A_LoopField%, %A_Space%
	if A_Index between 2 and 6
		GuiControl, 1:, ed%A_Index%, % cini%A_Index%
	}
GuiControl, 1:, ed1, %cini1% %cini9% %cini10%
GuiControl, 1:, ini0, %pkg%	; package name
;return

codeValidation:
test1 := test2 := ""
StringUpper, t, cini2
Loop, Parse, t
	if (A_LoopField=A_Space)
		test1 .= "-"
	else test1 .= Asc(A_LoopField) "."
StringTrimRight, test1, test1, 1
StringUpper, t, cini5
Loop, Parse, t
	if (A_LoopField=A_Space)
		test2 .= Asc(A_LoopField) "-"
	else test2 .= Asc(A_LoopField) "."
StringTrimRight, test2, test2, 1
;msgbox, cini2=%cini2%`ncini7=%cini7%`ntest1=%test1%`ncini5=%cini5%`ncini8=%cini8%`ntest2=%test2%
GuiControl, 1:, ed7, % (cini7 <> test1 ? "NO" : "YES")
GuiControl, 1:, ed8, % (cini8 <> test2 ? "NO" : "YES")
;return

iniValidation:
;return

bmpValidation:
IfNotExist, %src%\%ini2%
	{
	Gui, 1:Font, cRed Bold
	GuiControl, 1:Font, ini2
	Gui, 1:Font
	}
else
	{
	Gui, 1:Font
	GuiControl, 1:Font, ini2
	}
if tini3
	{
	IfNotExist, %src%\%ini3%
		{
		Gui, 1:Font, cRed Bold
		GuiControl, 1:Font, ini3
		Gui, 1:Font
		}
	else
		{
		Gui, 1:Font
		GuiControl, 1:Font, ini3
		}
	}
;return

checkmarks:
GuiControl, 1:, tini0, % pkg=ini8 ? 1 : 0
GuiControl, 1:, tini3, % ini3="" ? 0 : 1
GuiControl, 1:, tini7, % ini7="" ? 0 : 1
GuiControl, 1:, tini9, % ini9="" ? 0 : 1
GuiControl, 1:, tini10, % ini10="" ? 0 : 1
return
;==========================================================
;	PACK/UNPACK ROUTINES
;==========================================================
pack:
IfNotExist, %wd%\7z.exe
	FileInstall, res\7z.exe, %wd%\7z.exe
RunWait, %wd%\7z.exe a -tzip "%arch%.bootskin" %arcdir%\*, %wd%, Hide
msgbox, Error=%ErrorLevel%
return
;==========================================================
update:
IfNotExist, %wd%\7z.exe
	FileInstall, res\7z.exe, %wd%\7z.exe
; only update individual files that have been modified
RunWait, %wd%\7z.exe u -tzip "%arch%.bootskin" %arcdir%\*, %wd%, Hide
return
;==========================================================
unpack:
FileRemoveDir, %src%, 1
IfNotExist, %wd%\7z.exe
	FileInstall, res\7z.exe, %wd%\7z.exe
RunWait, %wd%\7z.exe e "%file%" -o"%src%" * -r -y, %wd%, Hide
FileSetAttrib, -R-H-S, %src%\*.*, 1, 1
return
;==========================================================
clean:
Loop, %src%\*.bootskin, 0, 1
	{
	RunWait, %wd%\7z.exe e "%A_LoopFileLongPath%" -o"%src%" * -r -y, %wd%, Hide
	FileSetAttrib, -R-H-S, %src%\*.*, 1, 1
	FileDelete, %A_LoopFileLongPath%
	goto clean
	}
; needs testing for multiple nested bootskin plus inside folders
Loop, %src%\*, 2, 1
	{
	if !A_LoopFileName
		break
	FileRemoveDir, %A_LoopFileLongPath%, 1
	}
return
;==========================================================
;	INSTALL SKIN TO OFFICIAL BOOTSKIN FOLDER
;==========================================================
install:
Gui, 1:Submit, NoHide
Gui, ListView, LV2
if !odname := tini0 ? ini8 : ini0		; output directory name
	{
	; prompt user to type a valid folder name or skin name
	MsgBox, 0x2010, Input error, % "Please type in a valid " (tini0 ? skin : package) " name!"
	return
	}
if !bsdir
	{
	MsgBox, 0x2021, Path error,
		(
		Stardock BootSkin is not installed.
		Install skin in current folder instead?
		`(affects subsequent installations`)
		)
	IfMsgBox Cancel
		return
	bsdir := A_ScriptDir
	}
Loop, % LV_GetCount()
	{
	LV_GetText(i, r2, 1)
	LV_GetText(e, r2, 2)
	LV_GetText(file, r2, 3)	; don't replace this with 'src' because it can be a different path!
	file .= "\" i "." e
	FileCopy, %file%, %bsdir%\skins\%odname%, 1
	}
ini=%bsdir%\skins\%odname%\bootskin.ini
; if editor checkbox is on, write editor info to ini
; write new ini info
return
;==========================================================
/*
RegDB Key: SYSTEM\CurrentControlSet\Services\BootScreen
RegDB Val: Boot Bus Extender
RegDB Name: Group
RegDB Root: 2
RegDB Key: SYSTEM\CurrentControlSet\Services\BootScreen
RegDB Val: 1
RegDB Name: Type
RegDB Root: 2
RegDB Key: SYSTEM\CurrentControlSet\Services\BootScreen
RegDB Val: 0
RegDB Name: Start
RegDB Root: 2
RegDB Key: SYSTEM\CurrentControlSet\Services\BootScreen
RegDB Val: \SystemRoot\System32\drivers\vidstub.sys
RegDB Name: ImagePath
RegDB Root: 2
RegDB Key: SYSTEM\CurrentControlSet\Services\BootScreen\Parameters
RegDB Val: 1
RegDB Name: Enabled
RegDB Root: 2
RegDB Key: SYSTEM\CurrentControlSet\Services\BootScreen\Parameters
RegDB Val: 0
RegDB Name: DirtyBoot
RegDB Root: 2

RegDB Key: .bootskin
RegDB Val: BootSkin.Document
RegDB Key: BootSkin.Document
RegDB Val: BootSkin Document
RegDB Key: BootSkin.Document\shell\open\command
RegDB Val: C:\PROGRA~1\STARDOCK\WINCUS~1\BOOTSKIN\BootSkin.exe %1
*/
;==========================================================
;	LAUNCH HYPERLINK(S)
;==========================================================
edlink:
if ed3
	Run, open %ed3%,, UseErrorLevel
return
;==========================================================
;	DISPLAY SELECTED IMAGE
;==========================================================
loadBmp:
IfNotExist, %file%
	return
GuiControl, 1:, gbimg, Image info:
Gui, 1:Font
GuiControl, 1:Font, ed3
GuiControl, 1:-g, ed3
Loop, Parse, labels2, CSV
	{
	GuiControl, 1:, lbl%A_Index%, % A_LoopField ? A_LoopField " :" : ""
	GuiControl, 1:Move, ed%A_Index%, w90
	}
GuiControl, 1:Show, bml1
GuiControl, 1:Show, bml2
GuiControl, 1:Disable, bml2
GuiControl, 1:Hide, tedit0
GuiControl, 1:Hide, ed8
FileRead, header, *c *m1142 %file%						; must use *c because of AHK_L
h1 := NumGet(header, 0, "UShort")						; bitmap identifier 424D (BM)
if (h1 = 0x4D42)
	{
	h2 := NumGet(header, 2, "UInt")						; file size
	h7 := NumGet(header, 18, "Int")						; bitmap width
	h8 := NumGet(header, 22, "Int")						; bitmap height
	h10 := NumGet(header, 28, "UShort")					; bits per pixel
	h11 := NumGet(header, 30, "UInt")					; compression method
	h13 := Round(NumGet(header, 38, "Int")*0.025400299)	; horizontal resolution [ppm to dpi]
	h14 := Round(NumGet(header, 42, "Int")*0.025400299)	; vertical resolution [ppm to dpi]
	h15 := NumGet(header, 46, "UInt")					; palette colors (0=default 2^n)
	bmpcol := h15 ? h15 : (h10 < 32 ? 2**h10 : 2**24)
	if bmpcol <=256
		{
		VarSetCapacity(xfile, 3*bmpcol, 0)
		Loop, %bmpcol%	; convert from BRG to RGB while transferring to buffer
			{
			NumPut(NumGet(header, 50+4*A_Index+2, "UChar"), xfile, 3*(A_Index-1), "UChar")
			NumPut(NumGet(header, 50+4*A_Index+1, "UChar"), xfile, 3*(A_Index-1)+1, "UChar")
			NumPut(NumGet(header, 50+4*A_Index, "UChar"), xfile, 3*(A_Index-1)+2, "UChar")
			}
		gosub buildPal
		GuiControl, 1:Enable, bml2
		}
	else if bmpcol <= 65536
		bmpcol := Round(bmpcol/1024) " k"
	else bmpcol := Round(bmpcol/1048576) "M"
	bmpcol := h10=32 ? bmpcol " + Alpha" : col
	x := pposX, y := pposY
	w := h := 0
	if (h7 >= bmW)
		w:=bmW
	else x := pposX+(bmW-h7)//2
	if (h8 >= bmH)
		h:=bmH
	else y := pposY+(bmH-h8)//2
	GuiControl, 1:MoveDraw, pic, x%x% y%y%
	GuiControl, 1:, ed1, Bitmap
	GuiControl, 1:, ed2, %h7%x%h8% px
	GuiControl, 1:, ed3, %h10% bpp
	Loop, Parse, comp, |
		if (h11 = A_Index-1)
			GuiControl, 1:, ed4, %A_LoopField%
	GuiControl, 1:, ed5, % (h13 && h14) ? h13 "x" h14 " dpi" : "unknown"
	GuiControl, 1:, ed6, % h2 " bytes"
	if (h7=640 && h8=480 && h10=4 && h11=0)
		GuiControl, 1:, ed7, YES
	}
else
	{
	w := bmW
	h := bmH
	Loop, 7
		GuiControl, 1:, ed%A_Index%,
	GuiControl, 1:, ed7, NO
	}
GuiControl, 1:, pic,					; attempt to free old bitmap handle
GuiControl, 1:, pic, *w%w% *h%h% %file%
GuiControl, 1:Show, pic
return
;==========================================================
;	BUILD PALETTE
;==========================================================
buildACT:
GuiControl, 1:, gbimg, Palette info:
Gui, 1:Font
GuiControl, 1:Font, ed3
GuiControl, 1:-g, ed3
Loop, Parse, labels3, CSV
	{
	GuiControl, 1:, lbl%A_Index%, % A_LoopField ? A_LoopField " :" : ""
	GuiControl, 1:Move, ed%A_Index%, w90
	}
GuiControl, 1:Show, bml1
GuiControl, 1:Show, bml2
GuiControl, 1:Enable, bml2
GuiControl, 1:Hide, pic		; maybe show a mini version of the palette instead?
GuiControl, 1:Hide, tedit0
GuiControl, 1:, ed7,
GuiControl, 1:Hide, ed8
FileRead, xfile, *c %file%

buildPal:
len	:= VarSetCapacity(xfile)
cols := 16									; best leave it at 16 OR read from ini
if (NumGet(xfile, 0, "UInt")=0x46464952 && NumGet(xfile, 8, "UInt64")=0x61746164204C4150)	; this is MS palette
	{
	palType := "Microsoft"
	palFmt := "RIFF"
	blk := 4
	sz := NumGet(xfile, 22, "UShort")
	rows	:= Ceil(sz/cols)
	VarSetCapacity(buffer, 4*rows*cols, 0)
	Loop, %sz%
		NumPut(NumGet(xfile, 24+4*(A_Index-1), "UInt"), buffer, 4*(A_Index-1), "UInt")
	}
else if (NumGet(xfile, 0, "UInt64")=0x4C41502D4353414A)	; this is JASC palette
	{
	FileRead, xfile, %file%	; required due to stupid Unicode parsing in AHK_L
	Loop, Parse, xfile, `n, `r
		{
		if A_Index=2
			ver := SubStr(A_LoopField, 1, 2)+0 "." SubStr(A_LoopField, 3)+0
		else if A_Index=3
			{
			sz := A_LoopField
			rows	:= Ceil(sz/cols)
			VarSetCapacity(buffer, 4*rows*cols, 0)
			}
		else if (sz && (A_Index>sz+3))
			break
		else if A_Index>3
			{
			StringSplit, v, A_LoopField, %A_Space%
			NumPut(v1+256*v2+0x10000*v3, buffer, 4*(A_Index-4), "UInt")
			blk := v0
			}
		}
	palType := "JASC " ver
	palFmt := "Text"
	}
else		; this might be Adobe/raw palette
	{
	blk	:= len=1024 ? 4 : 3		; 4/3 bytes per color
	sz := len>768 ? (len=1024 ? 256 : 256*NumGet(xfile, 768, "UChar")+NumGet(xfile, 769, "UChar")) : len//blk
	rows	:= Ceil(sz/cols)
	VarSetCapacity(buffer, 4*rows*cols, 0)
	Loop, % len/blk
		{
		i := NumGet(xfile, blk*(A_Index-1), "UInt")
		if blk=3
			i &= 0x00FFFFFF
		NumPut(i, buffer, 4*(A_Index-1), "UInt")
		}
	palType := "Adobe" (blk=3 && len=772 ? " CS2" : "/raw")
	palFmt := "Binary"
	}
VarSetCapacity(xfile, 0)	; how stupid is it to keep memory occupied, showing false size!?
;msgbox, Colors %sz% len=%len% blk=%blk% type %palType% cols=%cols% rows=%rows%
; read the following from ini OR settings dialog
g1 := -2, g2 := 15							; gap/margin
cellW := 20, cellH := 12						; color cell size w/h
ww	:= (cellW+4+g1)*cols+2*g2-g1+2		; add control borders if needed
wh	:= (cellH+4+g1)*rows+2*g2-g1+2		; add control borders if needed
array := cols "x" rows "x" sz					; columns/rows/number of colors
size	:= cellW "x" cellH
gap	:= g1 "x" g2
bkgClr := 0x000000				; background color LIGHTGRAY=0xD0D0D0
selClr := 0x0000FF					; selection color RED
buf := &buffer
PaletteStore(array, size, gap, buf, bkgClr, selClr, r, 1)	; store common values
GuiControl, 4:MoveDraw, Static1, w%ww% h%wh%
GuiControlGet, palpos, 4:Pos, Static1
GuiControl, 4:MoveDraw, Static2, % "y" palposY+palposH+2 " w" ww
GuiControlGet, palpos, 4:Pos, Static2
GuiControl, 4:MoveDraw, Edit1, % "y" palposY+palposH+2 " w" ww
GuiControl, 4:, Static2,
GuiControl, 4:, Edit1,
if DllCall("IsWindowVisible", "UInt", hPalWnd)	; if window is already visible,
	Gui, 4:Show, AutoSize NA			; resize and show it
if (A_ThisLabel <> "buildACT")		; don't change display values if palette
	return						; was loaded from a bitmap
GuiControl, 1:, ed1, %palType%
GuiControl, 1:, ed2, % blk*sz " bytes"
GuiControl, 1:, ed3, %sz%
GuiControl, 1:, ed4, %palFmt%
GuiControl, 1:, ed5, %blk%
GuiControl, 1:, ed6, %len% bytes
return
;==========================================================
;	SHOW PALETTE
;==========================================================
showpal:
if DllCall("IsWindowVisible", "UInt", hPalWnd)
	Gui, 4:Hide
	else Gui, 4:Show, AutoSize NA
return

4GuiClose:
Gui, 4:Hide
return
;==========================================================
;	(NON)ANIMATED PREVIEW
;==========================================================
preview:
Gui, 1:Submit, NoHide
if (!tini3 OR !ini3 OR (!FileExist(ini3) && !FileExist(src "\" ini3)))
	{
	GuiControl, 2:, pvw1, *w0 *h0 %src%\%ini2%
	GuiControl, 2:Hide, pvw2
	Gui, 2:Show, NA
	GuiControl, 1:, pvw, Preview
	stop=2
	return
	}
stop := !stop
GuiControl, 1:, pvw, % stop ? "Preview" : "Stop"
if stop
	{
	SetTimer, endless, Off
	return
	}
GuiControl, 2:, pvw1, *w0 *h0 %src%\%ini2%
GuiControl, 2:, pvw2, *w0 *h0 %src%\%ini3%
GuiControl, 2:MoveDraw, pvw2, x%ini4% y%ini5%
GuiControl, 2:Show, pvw2
Gui, 2:Show, NA
GuiControlGet, pb, 2:Pos, pvw2
th := pbW//3
xPvw := ini4
SetTimer, endless, %delay%
;msgbox, pbW=%pbW% pbH=%pbH% th=%th%
return

endless:
GuiControl, 2:MoveDraw, pvw2, x%xPvw% y%ini5%
xPvw += th
if (xPvw >= ini6+ini4-pbW)
	xPvw := ini4
return

delay:
if !stop
	SetTimer, endless, %delay%
return
;==========================================================
;	TOGGLE EDIT/VIEW MODE
;==========================================================
toggleMode:
mode := !mode
GuiControl, 1:, tev, % mode ? "View" : "Edit"
SB_SetText("`t" (mode ? "Edit" : "View") " mode", 1)
GuiControl, % "1:" (mode ? "Enable" : "Disable"), tini0
GuiControl, % "1:" (mode ? "Enable" : "Disable"), tini3
GuiControl, % "1:" (mode ? "Enable" : "Disable"), tini9
GuiControl, % "1:" (mode ? "Enable" : "Disable"), tini10
GuiControl, % "1:" (mode ? "Enable" : "Disable"), brbkg
GuiControl, % "1:" (mode ? "Enable" : "Disable"), brprg
GuiControl, % "1:" (mode && tini3 ? "Enable" : "Disable"), tini7
GuiControl, % "1:" (mode && tini3 && tini7 ? "Enable" : "Disable"), ini7
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini0
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini2
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini3
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini4
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini5
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini6
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini7
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini8
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini9
GuiControl, % "1:" (mode ? "-ReadOnly" : "+ReadOnly"), ini10
if mode
	{
	if tini3
		{
		ftmp := FileExist(ini3) ? ini3 : FileExist(src "\" ini3) ? src "\" ini3 : ""
		if ftmp
			{
			FileRead, tmp, *c *m26 %ftmp%
			iprW := NumGet(tmp, 18, "Int")			; progress bitmap width
			iprH := NumGet(tmp, 22, "Int")			; progress bitmap height
			VarSetCapacity(tmp, 0)					; free memory
			VarSetCapacity(ftmp, 0)					; free memory
			ifrH := (tini7 && ini7) ? ini7+2 : 2+iprH
			}
		else ifrH := 9+2			; default progress height is 9
		ifrX := ini4-1, ifrY := ini5-1, ifrW := ini6+2
		GuiControl, 2:MoveDraw, pvw3, x%ifrX% y%ifrY% w%ifrW% h%ifrH%
		GuiControl, 2:Show, pvw3
		}
	}
else GuiControl, 2:Hide, pvw3

return
;==========================================================
;	CHECKBOXES ACTION
;==========================================================
tini0:
Gui, 1:Submit, NoHide
GuiControl, % "1:" (tini0 ? "Disable" : "Enable"), ini0
return
tini3:
Gui, 1:Submit, NoHide
GuiControl, % "1:" (tini3 ? "Enable" : "Disable"), ini3
GuiControl, % "1:" (tini3 ? "Enable" : "Disable"), ini4
GuiControl, % "1:" (tini3 ? "Enable" : "Disable"), ini5
GuiControl, % "1:" (tini3 ? "Enable" : "Disable"), ini6
GuiControl, % "1:" (tini3 ? "Enable" : "Disable"), tini7
GuiControl, % "1:" (tini3 && tini7 ? "Enable" : "Disable"), ini7
GuiControl, % "1:" (tini3 ? "Enable" : "Disable"), brprg
return
tini7:
Gui, 1:Submit, NoHide
GuiControl, % "1:" (tini7 ? "Enable" : "Disable"), ini7
return
tini9:
Gui, 1:Submit, NoHide
GuiControl, % "1:" (tini9 ? "Enable" : "Disable"), ini9
return
tini10:
Gui, 1:Submit, NoHide
GuiControl, % "1:" (tini10 ? "Enable" : "Disable"), ini10
return
;==========================================================
;	PALETTE SELECT
;==========================================================
Select:
; check for out-of-color clicks and return if so
if !idx := PaletteSelect(array, size, gap)
	return
idx--
DllCall("SetCursor", "UInt", hCursH)
SetFormat, Integer, H
pix := SubStr("000000" SubStr(NumGet(buffer, 4*idx, "UInt"), 3), -5)
StringUpper, pix, pix
cBx := "0x" SubStr(pix, 1, 2)
cGx := "0x" SubStr(pix, 3, 2)
cRx := "0x" SubStr(pix, 5)
cBGR := "0x" pix
cRGB := "0x" SubStr(pix, 5) SubStr(pix, 3, 2) SubStr(pix, 1, 2)
SetFormat, Integer, D
cDec := "Red=" cRx+0 " Green=" cGx+0 " Blue=" cBx+0
GuiControl, 4:, Edit1, Index=%idx% RGB=%cRGB% BGR=%cBGR%
GuiControl, 4:, Static2, %cDec%
return
;==========================================================
options:
about:
msgbox, To be done!
return
;==========================================================
;	FUNCTIONS
;==========================================================
WM_MOUSEMOVE(wParam, lParam)
{
Global
Local wind, ctrl, hwnd, mX, mY
Static pf,hit,rect,isrect,v="H",v10="Z",v11="Z",v12="V",v13="TL",v14="TR",v15="V",v16="TR",v17="TL"
if !isrect
	isrect := VarSetCapacity(rect, 16, 0)
WinGetTitle, wind
MouseGetPos, mX, mY, hwnd, ctrl
; change StaticX below to suit the actual window(s)
if wind in %abtwin%
	if ctrl in Static4,Static9,Static11
		return DllCall("SetCursor", "UInt", hCursH)
if wind in %appname%
	{
	; URL field in Editor info group or Palette button
	if (ctrl = "Static20" && cini3 && e="ini") OR (ctrl = "Static26")
		return DllCall("SetCursor", "UInt", hCursH)
;	else if (ctrl = "Static1" && InStr(cpn, "noposter"))
;		return DllCall("SetCursor", "UInt", hCursN)
	}
if wind in Palette
	if ctrl in Static1
		return DllCall("SetCursor", "UInt", hCursH)
if wind = Preview
	{
	if mover1	; Edit mode allows static movement
		{
		GuiControl, 2:MoveDraw, pvw2, % "x" mx-deltaX " y" my-deltaY
		GuiControl, 2:MoveDraw, pvw3, % "x" mx-deltaX-1 " y" my-deltaY-1
		GuiControl, 1:, ini4, % mx-deltaX
		GuiControl, 1:, ini5, % my-deltaY
		Gui, 1:Submit, NoHide
		return DllCall("SetCursor", "UInt", hCursH)
		}
	if mover2	; Edit mode allows static movement
		{
		if !hit
			nx := mx-delta2X, ny := my-delta2Y, nw := frW, nh := frH
		if hit=10			; LEFT
			nx := mx-delta2X, ny := frY, nw := frW+frX-nx, nh := frH
		else if hit=12		; TOP
			nx := frX, ny := my-delta2Y, nw := frW, nh := frH+frY-ny
		else if hit=11		; RIGHT
			nx := frX, ny := frY, nw := mx-nx, nh := frH
		else if hit=15		; BOTTOM
			nx := frX, ny := frY, nw := frW, nh := my-ny
		else if hit=13		; TOP-LEFT
			nx := mx-delta2X, ny := my-delta2Y, nw := frW+frX-nx, nh := frH+frY-ny
		else if hit=14		; TOP-RIGHT
			nx := frX, ny := my-delta2Y, nw := mx-nx, nh := frH+frY-ny
		else if hit=16		; BOTTOM-LEFT
			nx := mx-delta2X, ny := frY, nw := frW+frX-nx, nh := my-ny
		else if hit=17		; BOTTOM-RIGHT
			nx := frX, ny := frY, nw := mx-nx, nh := my-ny
;gosub clipcurs
		GuiControl, 2:MoveDraw, pvw3, x%nx% y%ny% w%nw% h%nh%
		GuiControl, 1:, ini4, % nX+1
		GuiControl, 1:, ini5, % nY+1
		GuiControl, 1:, ini6, % nW-2
		GuiControl, 5:, Static4, Progress width=%iprW% height=%iprH%
		if (ifrH != nH)
			{
			GuiControl, 1:, ini7, % nH-2
			GuiControl, 1:Enable, ini7
			GuiControl, 1:, tini7, 1
			}
		else
			{
			GuiControl, 1:, ini7,
			GuiControl, 1:Disable, ini7
			GuiControl, 1:, tini7, 0
			}
		Gui, 1:Submit, NoHide
		curstype := v%hit%
		return DllCall("SetCursor", "UInt", hCurs%curstype%)
		}
	gosub startTimer
	if ctrl in Static2,Static3
		{
		if !mode
			return DllCall("SetCursor", "UInt", hCursN)
		hit := getcurs(hpvw3, resMgn)
		curstype := v%hit%
		return DllCall("SetCursor", "UInt", hCurs%curstype%)
		}
	if (pf != hPvw)
		{
		pf := DllCall("SetActiveWindow", "UInt", hPvw)
		f := DllCall("SetFocus", "UInt", hPvw)
		}
	return DllCall("SetCursor", "UInt", hCursA)
	}
return
}
/*
clipcurs:
if nh < iprH+2
	{
	nh := iprH+2
	maxX := mx
	}
if nw < iprW+2
	{
	nw := iprW+2
	maxY := my
	}
DllCall("SetCursorPos", "Int", maxX, "Int", maxY)
return
*/
startTimer:
if fscr
	SetTimer, hideCurs, % hcTime			; hide cursor in fullscreen on timer
else
	{
	SetTimer, hideCurs, Off
;	DllCall("ReleaseCapture")
	}
;GuiControl, 5:, Static1, Started timer fscr=%fscr%
return

hideCurs:
DllCall("SetCursor", "UInt", NULL)
;DllCall("SetCapture", "UInt", hMain)
;times++
;GuiControl, 5:, Static4, Hiding cursor %times%
return

;==========================================================
WM_LBUTTONDBLCLK(wParam, lParam)
{
Global
Local hwnd, ctrl
MouseGetPos,,, hwnd, ctrl
if (hwnd=hPvw && ctrl="Static1")
	{
	if fscr
		gosub restoreFS
	else gosub switchFS
;	fscr := !fscr
;SendMessage, 0x202, 0, mX+0xFFFF*mY, Static1, ahk_id %hwnd%	; WM_LBUTTONUP
	}
}
;==========================================================
WM_LBUTTONDOWN(wParam, lParam)
{
Global
Local hwnd, ctrl, mX, mY
MouseGetPos, mX, mY, hwnd, ctrl
if !mode
	return
if (hwnd=hPvw && ctrl="Static2")
	{
	DllCall("SetCursor", "UInt", hCursH)
	GuiControlGet, pb, 2:Pos, pvw2		; get progressbar position
	deltaX := mX-pbX, deltaY := mY-pbY
	mover1=1
	}
else if (hwnd=hPvw && ctrl="Static3")
	{
	DllCall("SetCursor", "UInt", hCurs%curstype%)
	GuiControlGet, fr, 2:Pos, pvw3		; get progress range position
	delta2X := mX-frX, delta2Y := mY-frY
	mover2=1
	}
else mover1 := mover2 := 0
}
;==========================================================
WM_LBUTTONUP(wParam, lParam)
{
Global
Local hwnd, ctrl
MouseGetPos,,, hwnd, ctrl
if !mode
	return
mover1=0
mover2=0
if (hwnd=hPvw && ctrl="Static2")
	{
	DllCall("SetCursor", "UInt", hCursH)
	}
else if (hwnd=hPvw && ctrl="Static3")
	{
	GuiControl, 2:MoveDraw, pvw2, % "x" ini4+ini6//2-iprW//2 " y" ini5+1
	DllCall("SetCursor", "UInt", hCurs%curstype%)
	}
else if (hwnd=hPvw)
	DllCall("SetCursor", "UInt", hCursA)
}
;==========================================================
getcurs(ctrl, rm="")
{
Critical
if rm
	rmX := rmY := rm
else
	{
	SysGet, rmX, 32
	SysGet, rmY, 33
	}
cursor=
MouseGetPos, mX, mY,, ctrl1, 2
if (ctrl != ctrl1)		; don't drag other items
	return cursor
ControlGetPos, wx, wy, ww, wh,, ahk_id %ctrl%
mX -= wx, mY -= wy
hm := ww-rmX
vm := wh-rmY
GuiControl, 5:, Static1, Frame wx=%wx% wy=%wy% ww=%ww% wh=%wh%
GuiControl, 5:, Static2, Mouse mx=%mx% my=%my%, hm=%hm% vm=%vm%
GuiControl, 5:, Static3, Margins rmx=%rmx% rmy=%rmy%
if mX between 0 and %rmX%
	{
	if mY between 0 and %rmY%
		cursor = 13		; HTTOPLEFT
	else if mY between %vm% and %wh%
		cursor = 16		; HTBOTTOMLEFT
	else
		cursor = 10		; HTLEFT
	}
else if mX between %hm% and %ww%
	{
	if mY between 0 and %rmY%
		cursor = 14		; HTTOPRIGHT
	else if mY between %vm% and %wh%
		cursor = 17		; HTBOTTOMRIGHT
	else
		cursor = 11		; HTRIGHT
	}
else if mY between 0 and %rmY%
		cursor = 12		; HTTOP
else if mY between %vm% and %wh%
		cursor = 15		; HTBOTTOM
GuiControl, 5:, Static3, Margins rmx=%rmx% rmy=%rmy% val=%cursor%
return cursor
}

;==========================================================
;	WINDOWPROC OVERRIDE
;==========================================================
WindowProc(hwnd, msg, wP, lP)
{
Global
Static hCWP
Critical
if !hCWP
	hCWP := DllCall("GetProcAddress", "UInt", DllCall("GetModuleHandle", "Str", "user32.dll"), AStr, "CallWindowProcA")
if (msg=0xF)	; WM_PAINT
	PalettePaint(hwnd)
return DllCall(hCWP, "UInt", WPOld, "UInt", hwnd, "UInt", msg, "UInt", wP, "UInt", lP)
}
;==========================================================
#include lib\func_FullScreen.ahk
#include lib\func_Palette.ahk
