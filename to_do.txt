* decompress .bootskin files
* check image parameters (type, size, color depth)
* display main image in preview (and/or error when appropriate)
* read and display ini information (notify bad string formattings such as extra/missing double-quotes)
- import/export ini from/to existing skin folder
- check progressbar image width against range to see if trick is used or is bad calculation
* animate skin (when available)
- install to Skins folder
---
/- L-click to set progressbar top-left corner (protect against min width unless trick is used)
/- Shift-click to set progressbar end (protect against max width)
/- Ctrl-click to set progressbar height
* manually position progressbar by dragging
---
- do not allow double-quotes in any string
- automatically double ampersands in strings
- write Editor section on demand
- write/fix extra/missing double-quotes in strings
- figure out validation encode/decode routine
* add text field/dialog for readme and other kind of text files
* add code to recognize (and display) Adobe Color Table (.act) files (256 color palette)
- add keyboard navigation for palette window
*- if preview window is open, update the bitmaps accordingly when new skin is selected
- hide cursor on timeout, in fullscreen preview
*- default mini-preview to main bitmap instead of bootskin.ini
- build Settings dialog for palette options and others
===
[Editor]
Name = "BootSkin Creator"
Author = "Drugwash"
Version = "%version%"
BuildType = "Alpha/Beta/Release/etc"
URL = "Author/Tool homepage"
SkinCode = "68.82.85.71.87.65.83.72"

[BootSkin]
Type = 0
Name = "Skin name"
Author = "Author name"
Description = "Description string"
Screen = Skin.bmp
ProgressBar = Progress.bmp
ProgressBarX = xxx
ProgressBarY = xxx
ProgressBarWidth = xxx
ProgressBarHeight = xxx

Windows\System32\drivers\vidstub.sys
9716 bytes of actual code.
BOOTPACK> PACKDATA > 0 --> header=52 bytes, followed by screenbmp followed by progress bmp.

4	identifier (00 48 52 49)		IRH
4	nmb of files? (00 00 00 02)	2
4	unknown (00 00 00 00)		0
4	resource size (00 02 59 8C)	153996
4	progress X (00 00 00 17)		23
4	progress Y (00 00 00 EC)	236
4	progress W (00 00 02 52)	594
4	progress H (00 00 00 09)	9
4	unknown (00 00 00 00)		0
4	bitmap offset (00 00 00 34)	52
4	bitmap size (00 02 58 76)	153718
4	progr. offset (00 02 58 AA)	153770
4	progress size (00 00 00 E2)	226
-bitmap-
-progress-
============================
.ACT file format
- 256 RGB colors
- 3 bytes per color
- index starts with zero
- CS2 added two 16bit values at end:
	- number of colors contained
	-???
============================
Color picker Static
0x5003.000B 0x4		WS_CHILD WS_VISIBLE WS_GT SS_SIMPLE WS_EX_NOPARENTNOTIFY
0x5000.100B 0x2.0004	WS_CHILD WS_VISIBLE SS_SUNKEN SS_SIMPLE WS_EX_STATICEDGE WS_EX_NOPARENTNOTIFY

Imagine palette Static
0x5001.1100 0x2.0004	WS_CHILD WS_VISIBLE WS_TABSTOP SS_SUNKEN SS_NOTIFY WS_EX_STATICEDGE WS_EX_NOPARENTNOTIFY